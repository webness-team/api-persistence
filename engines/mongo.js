module.exports 						= mongo;

var mongoDriver						= require('mongodb').MongoClient
  , dbPath                          = "mongodb://santidesimone1990:rickandmorty2000@ds157654.mlab.com:57654/"
  , dbName                          = 'webness-prototype'
  , dbUrl							= dbPath + dbName
  , dbInstance						= false
  , isConnecting					= false
  , dbQueue               = []

function mongo(dbName){
	this.db							    = dbInstance || false;
    if(!this.db && !isConnecting){
		  this.open();
    }
  this.ObjectId           = require('mongodb').ObjectId;
};

mongo.prototype.get				= function(options, callback){
  var collection      = options.collection || "",
      query = options.query || {},
      limit = options.limit || 0,
      sort =  options.sort  || {},
      findMethod = (options.query=="getById")?"findById":"find";
	if(this.db){
    if(query=="getCollectionsName"){
      this.db.listCollections().toArray(callback)
    }
    else{
      this.db.collection(collection)[findMethod](query).sort(sort).limit(limit).toArray(callback);
    }
	}
	else{
		dbQueue.push( { method:"get", options : options, callback : callback} )
	}
};

mongo.prototype.post				= function(options, callback){
  var collection      = options.collection || "",
      data = options.data || {};
	if(this.db){
		// console.log("Before Triger");
    this.db.collection(collection).insert(data, callback);
	}
	else{
		console.log("No Ready Yet post");
		dbQueue.push({method:"post", options:options, callback:callback})
	}
};

mongo.prototype.put        = function(options, callback){
  var collection  = this.db.collection(options.collection),
      query       = options.query || {},
      update      = options.update || {},
      options     = options.options || {};
  collection.update(query, {$set: update }, options, callback);
// https://docs.mongodb.com/v3.4/reference/method/db.collection.update/
};

mongo.prototype.delete			= function(options, callback){

  var collection  = this.db.collection(options.collection),
      query       = options.query || {},
      options     = options.options || {};
  collection.remove(query, options, callback);

// https://docs.mongodb.com/v3.4/reference/method/db.collection.remove/
//   db.collection.remove(
//      <query>,
//      {
//        justOne: <boolean>,
//        writeConcern: <document>,
//        collation: <document>
//      }
//   )

};

mongo.prototype.open				= function(){
	var self						= this
	  , triggerQueue				= function(){
	  		if(dbQueue.length>0 && self.db){
	  			dbQueue.forEach(function(itm, idx){
	  				self[itm.method](itm.options, itm.callback);
	  			});
	  		}
		};
	mongoDriver.connect(dbUrl, function (err, database) {
		if(err){
			console.dir("Database connection fail");
		}
		else{
			self.db				= database;
			console.dir("Database connection ready");
			triggerQueue();
		}
	});
};

// mongo.prototype.close	= function(){
//   this.db.close();
// };

