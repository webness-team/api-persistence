module.exports = firestore

const firebaseAdmin = require('firebase-admin');

function firestore(){}

firestore.prototype.init = function(options){
  const config = {
    credential: options.credential,
    databaseURL: options.databaseURL
  };
  firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(options.credential),
    databaseURL: 'https://fir-for-webness.firebaseio.com'
  });
  this.firestoreDb = firebaseAdmin.firestore();
  this.firestoreDb.settings({timestampsInSnapshots: true});
};
