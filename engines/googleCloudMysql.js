module.exports = googleCloudMysql

const mysql = require('mysql');

function googleCloudMysql(){}

googleCloudMysql.prototype.connect = function(options, callback){
  const config = {
    user: options.user,
    password:  options.password,
    database: options.database,
    host: options.host,
  };
  this.connection = mysql.createConnection(config);
  this.connection.connect(callback);
};

googleCloudMysql.prototype.query = function(query, timeout, values, callback){
  let options  = {
      sql: query
  };
  if(values){
    options.values = values; // ['David']
  };
  options.timeout = timeout || 40000;
  this.connection.query(options, callback);
};

googleCloudMysql.prototype.escape = function(data){
  return this.connection.escape(data);
};

googleCloudMysql.prototype.list = function(limit, token, callback){
  // token = token ? parseInt(token, 10) : 0;
  // let array = []; //[limit, token]
  // this.connection.query(
  //   'SELECT * FROM account', array ,
  //   (err, results) => {
  //     if (err) {
  //       callback(err);
  //       return;
  //     }
  //     // const hasMore = results.length === limit ? token + results.length : false;
  //     // callback(null, results, hasMore);
  //     callback(results);
  //   }
  // );
};

googleCloudMysql.prototype.create = function(data, callback){
    // this.connection.query('INSERT INTO `account` (NAME, ID) VALUES ("admin", 4)', data, (err, res) => {
    //   if (err) {
    //     callback(err);
    //     return;
    //   }
    //   read(res.insertId, callback);
    // });
};

googleCloudMysql.prototype.read = function(id, callback){
  //   this.connection.query(
  //     'SELECT * FROM `account` WHERE `id` = ?', id, (err, results) => {
  //       if (!err && !results.length) {
  //         err = {
  //           code: 404,
  //           message: 'Not found'
  //         };
  //       }
  //       if (err) {
  //         callback(err);
  //         return;
  //       }
  //       callback(null, results[0]);
  //     });
};

googleCloudMysql.prototype.update = function(id, data, callback){
  //  this.connection.query(
  //     'UPDATE `account` SET ? WHERE `id` = ?', [data, id], (err) => {
  //       if (err) {
  //         callback(err);
  //         return;
  //       }
  //       read(id, callback);
  //     });
};

googleCloudMysql.prototype._delete = function(id, callback){
  //  this.connection.query('DELETE FROM `account` WHERE `id` = ?', id, callback)
};

// googleCloudMysql.end(function(err) {
//   // The connection is terminated now
// });
