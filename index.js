const googleCloudMysql = require('./engines/googleCloudMysql');
const firestore = require('./engines/firestore');
const mongo = require('./engines/mongo');

module.exports = {
  googleCloudMysqlApi : googleCloudMysql,
  firestoreApi : firestore,
  mongoApi : mongo,
}
